// Config environment
const express = require("express"); // Import express
const app = express(); // Make express app

/* Enables req.body */
app.use(express.json()); // Enables req.body (JSON)
// Enables req.body (url-encoded)
app.use(
  express.urlencoded({
    extended: true,
  })
);

function sortStr(str) {
  const newStr = str.split("");
  for (let i = 0; i < newStr.length; i++) {
    for (let j = i; j < newStr.length; j++) {
      if (newStr[i] > newStr[j]) {
        let temp = newStr[i];
        newStr[i] = newStr[j];
        newStr[j] = temp;
      }
    }
  }
  return newStr.join("");
}

app.post("/", (req, res) => {
  let name = req.body.name;

  res.json({ message: sortStr(name) });
});

/* Run the server */
if (process.env.NODE_ENV || process.env.PORT !== "test") {
  app.listen(3000, () => console.log(`Server running on 3000`));
}

module.exports = app;
