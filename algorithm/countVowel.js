// defining vowels
const vowels = ["a", "e", "i", "o", "u"];

function countAlphabet(str) {
  // initialize count
  let countVowel = 0;
  let countConsonant = 0;
  const check = {};
  // loop through string to test if each character is a vowel
  for (let character of str.toLowerCase()) {
    if (vowels.includes(character) && !check[character]) {
      countVowel++;
      check[character] = true;
      // loop through string to test if each character is a consonant
    } else if (!vowels.includes(character) && isLetter(character)) {
      countConsonant++;
    }
  }

  // return number of vowels
  return `hasil: "huruf hidup: ${countVowel}, huruf mati: ${countConsonant}"`;
}

function isLetter(c) {
  return c.toLowerCase() !== c.toUpperCase();
}

console.log(countAlphabet("omama"));
console.log(countAlphabet("otomatis"));
